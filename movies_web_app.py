import os
import time
import datetime

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    # db='web_app'
    # username='root' 
    # password='palak123'
    # hostname='localhost'
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT'\
                + ', year INT' \
                + ', title TEXT' \
                + ', director TEXT' \
                + ', actor TEXT' \
                + ', release_date TEXT' \
                + ', rating DECIMAL(4,2)' \
                + ',PRIMARY KEY (id))'  

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        #populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def query_movie_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_movie_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movies")
    entries = [dict(movie=row) for row in cur.fetchall()]
    return entries

    
try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/test')
def test():
    print("HEEE")
    return 'Hello, World'


def message_output(msg):
    #entries = query_movie_data()
    return render_template('index.html', message=msg)

def array_output(results, msg):
    #entries = query_movie_data()
    #return render_template('index.html', message=results)
    return render_template('index.html', message=msg, output=results)




@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Insert Movie Request Received.")
    year = request.form['year']
    title = request.form['title'].title()
    director = request.form['director'].title()
    actor = request.form['actor'].title()
    release_date = request.form['release_date']
    rating = request.form['rating']

    if not title:
        return message_output("Movie " + title + " could not be inserted - Title not Entered")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    success_msg = ''
    try:
        release_date = datetime.datetime.strptime(release_date, '%d %b %Y')
        cur = cnx.cursor()
        cur.execute("SELECT title FROM movies where title = '" + title + "'")
        entries = cur.fetchall()        
        if len(entries):
             return message_output("Movie " + title + " could not be inserted - Movie Already Exists")
        else:
            cur.execute("INSERT INTO movies (year, title, director, actor, release_date, rating) values ('" + year + "','" + title + "','" + director + "','" +  actor + "','" + str(release_date) + "','" + rating + "')")
            success_msg = "Movie " + title + " successfully inserted"
        cnx.commit()
    except Exception as exp:
        success_msg = "Movie " + title + " could not be inserted - " + str(exp)
    return message_output(success_msg)


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Update Movie Request Received.")
    year = request.form['year']
    title = request.form['title'].title()
    director = request.form['director'].title()
    actor = request.form['actor'].title()
    release_date = request.form['release_date']
    rating = request.form['rating']

    if not title:
        return message_output("Movie " + title + " could not be update - Title not Entered")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        print(exp)

    success_msg = ''
    try:
        release_date = datetime.datetime.strptime(release_date, '%d %b %Y')
        cur = cnx.cursor()
        cur.execute("SELECT id, title FROM movies where title = '" + title + "'")
        entries = cur.fetchall()        
        if len(entries):
            movie_id = str(entries[0][0])
            cur.execute("UPDATE movies SET year = '" + str(year) + "',title='" + str(title) + "',director='" + str(director) + "',actor='" + str(actor) + "',release_date='" + str(release_date) + "',rating='" + str(rating) + "' where id = " + "'" + movie_id + "'")
            success_msg = "Movie " + title + " successfully updated"
        else:
            return message_output("Movie " + title + " could not be updated - Movie does not Exist")
        cnx.commit()
    except Exception as exp:
        success_msg = "Movie " + title + " could not be updated - " + str(exp)
    return message_output(success_msg)

@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    print("Delete Movie Request Received.")
    print("Delete Title: ", request.form['delete_title'])
    success_msg = ''
    title = request.form['delete_title'].title()
    

    if not title:
        return message_output("Movie " + title + " could not be deleted - Title not Entered")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    success_msg = ''
    try:
        cur = cnx.cursor()
        cur.execute("SELECT id, title FROM movies where title = '" + title + "'")
        entries = cur.fetchall()        
        if len(entries):
            movie_id = str(entries[0][0])
            cur.execute("DELETE from movies where id = '" + movie_id + "'")
            success_msg = "Movie " + title + " successfully deleted"
        else:
            success_msg = "Movie with title " + title + " does not exist"
        cnx.commit()
    except Exception as exp:
        success_msg = "Movie " + title + " could not be deleted - " + str(exp)
    
    return message_output(success_msg)

@app.route('/search_movie', methods=['GET'])
def search_movie():
    print("Search Movie Request Received.")
    print("Search Movie Actor: ", request.args.get('search_actor'))
    success_msg = ''
    actor = request.args.get('search_actor').title()
    

    if not actor:
        return message_output("Actor Name not Entered")

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, movies.year, actor from movies where actor = '" + actor + "'")
        entries = cur.fetchall()
        if len(entries):
            result = []
            for entry in entries:
                temp_str = str("Title: " + entry[0]) + ", Year: "  + str(entry[1]) + ", Actor: " + str(entry[2])
                result.append(temp_str)

            return array_output(result, "Movies with actor " + actor + ":")
        else:
            return message_output("No movies found for actor " + actor)
    except Exception as exp:
        success_msg = "Movie actor" + actor + " search could not be done - " + str(exp)
        return message_output(success_msg)
    

@app.route('/highest_rating', methods=['GET'])
def highest_rating():
    print("Highest Rating Movie Request Received.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, movies.year, actor, director, rating from movies where rating = (select max(rating ) from movies)")
        entries = cur.fetchall()
        if len(entries):
            result = []
            for entry in entries:
                temp_str = str("Title: " + entry[0]) + ", Year: "  + str(entry[1]) + ", Actor: " + str(entry[2]) + ", Director: " + str(entry[3]) + ", Rating: " + str(entry[4])
                result.append(temp_str)

            return array_output(result, "Highest Rating Movie(s): ")
        else:
            return message_output("Error getting movies with the highest rating - No movies in the table")
    except Exception as exp:
        msg = "Error getting movies with the highest rating - " + str(exp)
        return message_output(msg)


@app.route('/lowest_rating', methods=['GET'])
def lowest_rating():
    print("Lowest Rating Movie Request Received.")
    db, username, password, hostname = get_db_creds()
    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, movies.year, actor, director, rating from movies where rating = (select min(rating ) from movies)")
        entries = cur.fetchall()
        if len(entries):
            result = []
            for entry in entries:
                temp_str = str("Title: " + entry[0]) + ", Year: "  + str(entry[1]) + ", Actor: " + str(entry[2]) + ", Director: " + str(entry[3]) + ", Rating: " + str(entry[4])
                result.append(temp_str)
            return array_output(result, "Lowest Rating Movie(s): ")
        else:
            return message_output("Error getting movies with the lowest rating - No movies in the table")
    except Exception as exp:
        msg = "Error getting movies with the lowest rating - " + str(exp)
        return message_output(msg)




@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_movie_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
